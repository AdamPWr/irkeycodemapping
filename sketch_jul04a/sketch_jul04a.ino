// nadajnik musi byc podłączony do pinu nr 3 


#include <IRremote.h>
#define irPin 11
const int Button1 = 4;
const int Button2 = 5;
const int Button3 = 6;
IRrecv irrecv(irPin);
decode_results results;

IRsend irsend;

int wlacznik, b1,b2,b3,b4,b5,b6,b7,b8,b9,up,down,right,left,key;


void maping();
int getKeyCode();
void identyfy();
int waitForButtonPressed();

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn();
  while (!Serial);
  Serial.println("Receiver enabled");
  maping();

  // test przyciskow
  pinMode(Button1,INPUT);
  pinMode(Button2,INPUT);
  pinMode(Button3,INPUT);

}

void loop()
{   
  //waitForButtonPressed();
  //delay(200);
}

// pobiera kody z pilota i zapisuje do odpowiednich zmiennych
// koleiność wciskania pojawia się na serial monitorze
void maping()
{
  Serial.println("Włacznik");
  wlacznik = getKeyCode();
  Serial.println("1");
  b1 = getKeyCode();
  Serial.println("2");
  b2 = getKeyCode();
  Serial.println("3");
  b3 = getKeyCode();
  Serial.println("4");
  b4 = getKeyCode();
  Serial.println("5");
  b5 = getKeyCode();
  Serial.println("6");
  b6 = getKeyCode();
  Serial.println("7");
  b7 = getKeyCode();
  Serial.println("8");
  b8 = getKeyCode();
  Serial.println("9");
  b9 = getKeyCode();
  Serial.println("up");
  up = getKeyCode();
  Serial.println("down");
  down = getKeyCode();
  Serial.println("right");
  right = getKeyCode();
  Serial.println("left");
  left = getKeyCode();
}

// zwraca kod odebrany w odbiorniku podrzerwieni 
inline int getKeyCode()
{
  while(true)
  {
    if (irrecv.decode(&results))
    {
       Serial.println(results.value);
       delay(250);
       irrecv.resume(); 
       return results.value;  
    }
    else continue;
  }
}

// identyfikuje ktory klawisz zostal wcisniety i wyswietla jego nazwe na serial monitorze 
void identyfy()
{
    key = getKeyCode();
  
    if(key == wlacznik)
    {
      Serial.println("Wcisnoles  wlacznik");
    }
    else if( key == b1)
    {
      Serial.println("Wcisnoles 1");
    }
    else if( key == b2)
    {
      Serial.println("Wcisnoles 2");
    }
    else if( key == b3)
    {
      Serial.println("Wcisnoles 3");
    }
    else if( key == b4)
    {
      Serial.println("Wcisnoles 4");
    }
    else if( key == b5)
    {
      Serial.println("Wcisnoles 5");
    }
    else if( key == b6)
    {
      Serial.println("Wcisnoles 6");
    }
    else if( key == b7)
    {
      Serial.println("Wcisnoles 7");
    }
    else if( key == b8)
    {
      Serial.println("Wcisnoles 8");
    }
    else if( key == b9)
    {
      Serial.println("Wcisnoles 9");
    }
    else if( key == up)
    {
      Serial.println("Wcisnoles up");
    }
    else if( key == down)
    {
      Serial.println("Wcisnoles down");
    }
    else if( key == right)
    {
      Serial.println("Wcisnoles right");
    }
    else if( key == left)
    {
      Serial.println("Wcisnoles left");
    }
    else 
    {
      Serial.println("Wcisnales nie zmapowany klawisz");
    }
}

// zwraca kod nacisnietego klawisza
// po wywolaniu funkcji dobrze jest zastosowac opoźnienie
int waitForButtonPressed()
{
  while(true)
  {
    if(digitalRead(Button1) == LOW)
    {
      Serial.println("Zostal wcisniety przycisk nr "+ String(Button1));
      return Button1;
    }
    else if (digitalRead(Button2) == LOW)
    {
      Serial.println("Zostal wcisniety przycisk nr "+ String(Button2));
      return Button2;
    }
    else if (digitalRead(Button3) == LOW)
    {
      Serial.println("Zostal wcisniety przycisk nr "+ String(Button3));
      return Button3;
    }
    else delay(50);
  }
}
/* MAPOWANIE NA PILOCIE OD TV WE WROCŁAWIU
 Włacznik
1634748599
1
1634762879
2
1634746559
3
1634779199
4
1634738399
5
1634771039
6
1634754719
7
1634787359
8
1634734319
9
1634766959
up
1634783279
down
1634742479
right
1634744519
left
1634785319

*/
 
